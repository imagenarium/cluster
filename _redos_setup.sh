#!/bin/bash

source /tmp/install.conf &> /dev/null || true
source ./install.conf &> /dev/null || true

echo "========================================================================================"
echo "Install system packages"
echo "========================================================================================"

dnf -y install yum-utils mc

echo "========================================================================================"
echo "Install docker-ce"
echo "========================================================================================"

if [ -x "$(command -v docker)" ]; then
  echo "Docker already installed: $(docker --version)"
else
  dnf install -y docker-ce docker-ce-cli
  mkdir /root/.docker || true

  if [[ "${DOCKER_PATH}" ]]; then
    echo "========================================================================================"
    echo "Configure docker path: ${DOCKER_PATH}"
    echo "========================================================================================"

    mkdir -p /etc/docker || true
    echo -e "{\n  \"data-root\" : \"${DOCKER_PATH}\"\n}"  > /etc/docker/daemon.json
  fi

  systemctl restart docker && systemctl enable docker
fi

echo "========================================================================================"
echo "Configure kernel params"
echo "========================================================================================"

if [ ! -f "/etc/sysctl.d/60-imagenarium.conf" ]; then
  sysctl -w vm.swappiness=0
  echo "vm.swappiness=0" >> /etc/sysctl.d/60-imagenarium.conf
fi

echo "========================================================================================"
echo "Configure GRO"
echo "========================================================================================"

if [ ! -f "/etc/default/gro_off.sh" ]; then
  if [[ "${NETWORK_GRO}" == "off" ]]; then
    echo "[Unit]
    Description=GRO Off Service
    After=network.target
    [Service]
    ExecStart=/etc/default/gro_off.sh
    Type=oneshot
    User=root
    [Install]
    WantedBy=multi-user.target" | sed 's/^ *//' > /etc/systemd/system/gro.service

    echo "#!/bin/bash
    ethtool -K ${ADVERTISE_IF} gro off
    ethtool -K ${DATA_PATH_IF} gro off" | sed 's/^ *//' > /etc/default/gro_off.sh

    chmod 755 /etc/default/gro_off.sh
    chmod +x /etc/default/gro_off.sh
    systemctl enable gro && systemctl start gro
  fi
fi
