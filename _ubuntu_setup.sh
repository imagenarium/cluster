#!/bin/bash

source /tmp/install.conf &> /dev/null || true
source ./install.conf &> /dev/null || true

echo "========================================================================================"
echo "Install system packages"
echo "========================================================================================"

apt-get install -y mc curl ca-certificates gnupg2 lsb-release software-properties-common

echo "========================================================================================"
echo "Install docker-ce"
echo "========================================================================================"

if [ -x "$(command -v docker)" ]; then
  echo "Docker already installed: $(docker --version)"
else
  mkdir /root/.docker || true
  install -m 0755 -d /etc/apt/keyrings
  curl -fsSL https://download.docker.com/linux/ubuntu/gpg -o /etc/apt/keyrings/docker.asc
  chmod a+r /etc/apt/keyrings/docker.asc
  echo "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.asc] https://download.docker.com/linux/ubuntu \
$(. /etc/os-release && echo "$VERSION_CODENAME") stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

  apt-get update
  apt-get install -y docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin

  if [[ "${DOCKER_PATH}" ]]; then
    echo "========================================================================================"
    echo "Configure docker path: ${DOCKER_PATH}"
    echo "========================================================================================"

    mkdir -p /etc/docker || true
    echo -e "{\n  \"data-root\" : \"${DOCKER_PATH}\"\n}"  > /etc/docker/daemon.json
  fi

  systemctl restart docker && systemctl enable docker
fi

echo "========================================================================================"
echo "Configure kernel params"
echo "========================================================================================"

if [ ! -f "/etc/sysctl.d/60-imagenarium.conf" ]; then
  sysctl -w vm.swappiness=0
  echo "vm.swappiness=0" >> /etc/sysctl.d/60-imagenarium.conf
fi

echo "========================================================================================"
echo "Configure GRO"
echo "========================================================================================"

if [ ! -f "/etc/default/gro_off.sh" ]; then
  if [[ "${NETWORK_GRO}" == "off" ]]; then
    echo "[Unit]
    Description=GRO Off Service
    After=network.target
    [Service]
    ExecStart=/etc/default/gro_off.sh
    Type=oneshot
    User=root
    [Install]
    WantedBy=multi-user.target" | sed 's/^ *//' > /etc/systemd/system/gro.service

    echo "#!/bin/bash
    ethtool -K ${ADVERTISE_IF} gro off
    ethtool -K ${DATA_PATH_IF} gro off" | sed 's/^ *//' > /etc/default/gro_off.sh

    chmod 755 /etc/default/gro_off.sh
    chmod +x /etc/default/gro_off.sh
    systemctl enable gro && systemctl start gro
  fi
fi
